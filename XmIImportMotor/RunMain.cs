﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;



namespace XmIImportMotor
{
    static class RunMain
    {

        private static Definitions definitions = null;
        private static System.Timers.Timer aTimer;      
        private static bool flag_exportCliente = true;
        private static int timer_Interval = 3600000;
        private static Ini.IniFile file = null;
      
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            definitions = new Definitions();
            definitions.setConnection();

            file = new Ini.IniFile(Application.StartupPath + "/config.ini");
            timer_Interval = int.Parse(file.IniReadValue("PROCESS", "TIMERINTERVAL"));

            if (definitions.debugFlag == "0")
            {
                
                flag_exportCliente = true;

                aTimer = new System.Timers.Timer();
                aTimer.Enabled = false;
                aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                aTimer.Interval = 10000;
                aTimer.Enabled = true;

                while(true){System.Threading.Thread.Sleep(10000);}
            }

            if (definitions.debugFlag == "1")
            {
                flag_exportCliente = false;
                GetClientesImport();              
            }
        }
     
        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {

            aTimer.Interval = timer_Interval; /* 1 hora */
            System.GC.Collect();

            /// <summary>
            /// inicia processo hora-hora > exportacao por cliente com xml pendentes e exportação para portais (41)
            /// </summary>
            #region EXPORT-CLIENTE

            if (flag_exportCliente == true)
            {
                Utils.escreveLog("[INICIA-PROCESS] Import CLIENTE", "");

                aTimer.Enabled = false;
                GetClientesImport();

                //int x = 0;
                //while (x < 1000)
                //{
                //    Console.WriteLine(x);
                //    x++;
                //}

                aTimer.Enabled = true;
                Utils.escreveLog("[TERMINA-PROCESS] Import CLIENTE", "");
            }
            #endregion EXPORT-CLIENTE


            /// <summary>
            /// inicia processo horario noturno > importa xml + exportacao portais todos os clientes 
            /// agenda proxima exportacao total + 24horas
            /// </summary>
            #region EXPORT-TOTAL

            /* verifica proxima data exportação total */
            string dataExportNext = file.IniReadValue("PROCESS", "NEXTDATETOTAL");
            string horaExportNext = file.IniReadValue("PROCESS", "NEXTHOURTOTAL");
            string flagExportTotal = file.IniReadValue("PROCESS", "EXPTOTAL");

            DateTime dataAgora = DateTime.Parse("1900-01-01 00:00:00");
            DateTime.TryParse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), out dataAgora);
            
            DateTime dataExporta;
            DateTime.TryParse(dataExportNext, out dataExporta);

            int result = DateTime.Compare(dataAgora, dataExporta);

           if (result==1 && flagExportTotal=="1")
            {                             
                Utils.escreveLog("[INICIA-PROCESS] Import -- TOTAL --", "");

                try
                {
                    /* impedir que o processo de export total seja executado 2 vezes se falhar */                    
                    file.IniWriteValue("PROCESS", "EXPTOTAL", "0");
                    flag_exportCliente = false;
                    aTimer.Enabled = false;

                    GetClientesImport();

                    try
                    {
                        Process _processExport = new Process();
                        _processExport.StartInfo.FileName = definitions.nextProcess_path_1;
                        //_processExport.StartInfo.FileName = "C:\\Projectos\\ExportPortais\\XmlExportPortais\\XmlExportPortais\\bin\\Debug\\XmlExportPortais.exe ";
                        _processExport.Start();
                        _processExport.Refresh();
                        _processExport.WaitForExit();

                        Utils.escreveLog("[TERMINA-PROCESS] Import -- TOTAL --", "");

                    }
                    catch (Exception ex)
                    {
                        Utils.escreveLog("[EXPORT-TOTAL] " + ex, "ERRO");
                    }

                    //int x = 0;
                    //while (x < 10000)
                    //{
                    //    Console.WriteLine(x);
                    //    x++;
                    //}

                    aTimer.Enabled = true;
                    /* agenda proxima data export total > activa novamente flag_exportCliente (hora/hora) */
                    dataExportNext = dataExporta.Date.AddDays(1).ToString("yyyy-MM-dd "+ horaExportNext);
                    file.IniWriteValue("PROCESS", "NEXTDATETOTAL", dataExportNext);
                    file.IniWriteValue("PROCESS", "EXPTOTAL", "1");
                    flag_exportCliente = true;

                }
                catch (Exception ex) { Utils.escreveLog("[ERRO-EXPTOTAL] " + ex, "ERRO"); }
            }

            #endregion EXPORT-TOTAL      
        }

        public static void GetClientesImport()
        {
            SqlConnection connection = null;
            SqlCommand command = null;

            try
            {
                connection = new SqlConnection(Utils.GetConnString());
                command = connection.CreateCommand();
                command.Connection.Open();
            }
            catch (Exception ex) { Utils.escreveLog("[ERRO] " + ex, "ERRO"); }

            string WHERE = "";
            if (definitions.clienteId == "")
                WHERE = " WHERE IA.activo = 1 AND C.ACTIVO = '1' AND IAF.importado = '0' "+ 
                    " GROUP BY IA.id,IA.idCliente,C.nome,IA.id_clienteXimo,IA.idAgencia,IA.activo,IA.MapIMPxml,IAF.ficheiro" +
                        " ORDER BY IA.idCliente,IA.idAgencia,MAX(IAF.geradoXmlData) ASC;";
            else
                WHERE = " WHERE IAF.id_clienteXimo = " + definitions.clienteId + " AND IA.activo = 1 AND C.ACTIVO = '1' AND IAF.importado = '0' " +
                        " GROUP BY IA.id,IA.idCliente,C.nome,IA.id_clienteXimo,IA.idAgencia,IA.activo,IA.MapIMPxml,IAF.ficheiro" +
                        " ORDER BY IA.idCliente,IA.idAgencia,MAX(IAF.geradoXmlData) ASC;";

            string queryImpAgencias = " SELECT DISTINCT IA.id,IA.idCliente AS CLIENTID_key,C.nome AS CLINOME,IA.id_clienteXimo AS CLIENTID_keyXimo,IA.idAgencia,IA.activo,IA.MapIMPxml,IAF.ficheiro AS ImportFile,MAX(IAF.geradoXmlData) AS geradoXmlData " +
                                      " FROM IMPORTACAO_AGENCIAS IA " +
                                      " INNER JOIN CLIENTES C ON C.id = idCliente " +
                                      " INNER JOIN IMPORTACAO_AGENCIAS_FILES IAF ON IAF.idCliente = C.id AND IAF.id_clienteXimo = C.ximo_id "
                                      + WHERE;

            string UrlXMLdata = "";

         
            DataTable dtImpClientes = Utils.SQL_OPEN(queryImpAgencias, command);
            dtImpClientes.Dispose();
            foreach (DataRow rows in dtImpClientes.Rows)
            {
                try
                {
                    XmlSql_IMPORT bulkMerge = null;

                    /* map estrutura xml por exportador default */
                    string pathXMLmapFields = Application.StartupPath + "\\configmap\\" + rows["MapIMPxml"].ToString() + "";

                    XElement xmlNodesMap;

                    if (File.Exists(pathXMLmapFields))
                        xmlNodesMap = XElement.Load(pathXMLmapFields);
                    else
                    {
                        Utils.escreveLog("[MAP FILE NAO EXISTE] : " + pathXMLmapFields, "ERRO");
                        continue;
                    }

                    UrlXMLdata = definitions.Filedir + rows["ImportFile"].ToString() + "";
                    XElement xmlNodesData;

                    if (File.Exists(UrlXMLdata))
                    {
                        #region backup ultima exportacao
                        string data_agora = DateTime.Now.ToString("yyyyMMddHHmmss");                       
                        System.IO.File.Copy(UrlXMLdata, Application.StartupPath + "\\dataxml\\backup\\" + rows["ImportFile"].ToString().Replace(".xml", "") + "_BK_" + data_agora + ".xml", true);
                        #endregion backup ultima exportacao

                        bulkMerge = new XmlSql_IMPORT();
                        bulkMerge.connection = connection;
                        bulkMerge.command = command;
                        bulkMerge.getSqlTablesClean();

                        xmlNodesData = XElement.Load(UrlXMLdata);

                        bulkMerge.ClienteId_Key = rows["CLIENTID_key"].ToString();
                        bulkMerge.ClienteId_KeyXimo = rows["CLIENTID_keyXimo"].ToString();

                        /* IMPORTAR POR AGENCIA */
                        dynamic queryXMLAgencias = from xNode in xmlNodesData.Elements("agencias").Elements("agencia")
                                                    select xNode;

                        foreach (XElement xEle in queryXMLAgencias)
                        {
                            try
                            {
                                Utils.escreveLog("[INI] Cliente: " + rows["CLIENTID_key"].ToString() + " Agencia: " + xEle.Element("id").Value + " XML: " + rows["ImportFile"].ToString(), "");
                                Console.WriteLine(" - AGENCIA - " + xEle.Element("id").Value + " File: " + rows["ImportFile"].ToString());
                                bulkMerge.AgenciaId_Key = xEle.Element("id").Value;
                                bulkMerge.getTablesImport_v2(xmlNodesMap, xmlNodesData);
                            }
                            catch (Exception ex)
                            {
                                Utils.escreveLog("[ERRO]" + ex.ToString() +" Cliente "+ rows["CLIENTID_key"].ToString() + " File: " + rows["ImportFile"].ToString(), "ERRO");                              
                            }
                        }
                    }
                    else
                    {
                        Utils.escreveLog("[IMPORT FILE NAO EXISTE] : Cliente " + rows["CLIENTID_key"].ToString() + " - " + rows["CLINOME"].ToString(), "ERRO");
                        continue;
                    }

                    if (File.Exists(UrlXMLdata))
                    {
                        try
                        {
                            /* CADA FICHEIRO PODE CONTER APENAS 1 OU VARIAS AGENCIAS > NOME = FICHEIRO_IDAGENCIA.XML = ficheiro='" + rows["ImportFile"].ToString() > NÃO USAR O ID DA AGENCIA = 1 */                          
                            Utils.SQL_EXEC("UPDATE IMPORTACAO_AGENCIAS_FILES SET importado ='1',importadoData=GETDATE() WHERE IMPORTADO=0 AND idCliente='" + rows["CLIENTID_key"].ToString() + "' AND ficheiro='" + rows["ImportFile"].ToString() + "';", command, 0);

                            #region exporta-cliente EventTimer

                            if (definitions.debugFlag == "0" && flag_exportCliente == true)
                            {
                                try
                                {
                                    Utils.escreveLog("[INI] Export Portais Cliente:"+ rows["CLIENTID_key"].ToString(), "");
                                    Process _processExport = new Process();                                   
                                    _processExport.StartInfo.FileName = definitions.nextProcess_path_1;
                                    //_processExport.StartInfo.FileName = "C:\\Projectos\\ExportPortais\\XmlExportPortais\\XmlExportPortais\\bin\\Debug\\XmlExportPortais.exe ";
                                    _processExport.StartInfo.Arguments = "51 " + rows["CLIENTID_keyXimo"].ToString(); /* id-exportador + id_cliente-ximo */
                                    _processExport.Start();
                                    _processExport.Refresh();
                                    _processExport.WaitForExit();

                                }
                                catch (Exception ex)
                                {
                                    Utils.escreveLog("[ERRO] " + ex, "ERRO");                                   
                                }
                            }

                            #endregion exporta-cliente EventTimer

                        }
                        catch (Exception ex)
                        {
                            Utils.escreveLog("[ERRO UPDATE IMPORT]" + ex.ToString(), "ERRO");                           
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utils.escreveLog("[ERRO] XMLFILE " + UrlXMLdata + " " + ex.ToString(), "ERRO");
                    string data_agora = DateTime.Now.ToString("yyyyMMddHHmmss");
                    string UrlXMLdata_BK = UrlXMLdata.Replace(".xml", "");
                    UrlXMLdata_BK = UrlXMLdata_BK + "_BK_ERRO_" + data_agora + ".xml";
                    System.IO.File.Copy(UrlXMLdata, UrlXMLdata_BK, true);
                }

                Utils.escreveLog("[FIM]", "");
            }                         
        }
    }
}
