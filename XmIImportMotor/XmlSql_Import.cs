﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace XmIImportMotor
{
    class XmlSql_IMPORT
    {
       
        public SqlConnection connection = null;
        public SqlCommand command = null;
        public DataTable dtSource;
        public DataTable mapFields;
        public bool flagDelete = false;
        public bool flagBackupImport = false;
        public string ClienteId_Key;
        public string AgenciaId_Key;
        public string ClienteId_KeyXimo;
       

        public bool creatSourceMapFields(XElement xmlNodes, string XMLnodeParent)
        {
                dtSource = new DataTable();
                mapFields = new DataTable();

                #region set colunas DataSource        
                    DataColumn dtCol = new DataColumn();
                    dtCol.ColumnName = "copy";
                    this.mapFields.Columns.Add(dtCol);
                    dtCol = new DataColumn();
                    dtCol.ColumnName = "key";
                    this.mapFields.Columns.Add(dtCol);
                    dtCol = new DataColumn();
                    dtCol.ColumnName = "name";
                    this.mapFields.Columns.Add(dtCol);
                    dtCol = new DataColumn();
                    dtCol.ColumnName = "source";
                    this.mapFields.Columns.Add(dtCol);
                    dtCol = new DataColumn();
                    dtCol.ColumnName = "target";
                    this.mapFields.Columns.Add(dtCol);
                    dtCol = new DataColumn();
                    dtCol.ColumnName = "attribute";
                    dtCol.DataType = typeof(DataTable);           
                    this.mapFields.Columns.Add(dtCol);
                    dtCol = new DataColumn();
                    dtCol.ColumnName = "parent";
                    this.mapFields.Columns.Add(dtCol);
                    dtCol = new DataColumn();
                    dtCol.ColumnName = "type";
                    this.mapFields.Columns.Add(dtCol);                   
                #endregion set colunas DataSource

           
            dynamic queryResult = from xNode in xmlNodes.Elements(XMLnodeParent).Elements("column")
                                  where (string)xNode.Element("copy") == "1" || ((int)xNode.Element("key") > 0) //|| (string)xNode.Element("source") == ""
                                  select xNode;

            foreach (XElement xEle in queryResult)
            {
                
                    DataRow rowData = this.mapFields.NewRow();
                    rowData["copy"] = xEle.Element("copy").Value;
                    rowData["key"] = xEle.Element("key").Value;
                    rowData["name"] = xEle.Element("name").Value;
                    rowData["source"] = xEle.Element("source").Value;
                    rowData["target"] = xEle.Element("target").Value;
                    //rowData["attribute"] = xEle.Element("attribute").Value;
                    rowData["parent"] = xEle.Element("parent").Value;
                    rowData["type"] = xEle.Element("type").Value;
                   
                    #region set colunas table child atributos    
                    DataTable valIMPFieldsAttr = new DataTable();

                    // table child atritutos
                    dtCol = new DataColumn();
                    dtCol.ColumnName = "copy";
                    valIMPFieldsAttr.Columns.Add(dtCol);
                    dtCol = new DataColumn();
                    dtCol.ColumnName = "name";
                    valIMPFieldsAttr.Columns.Add(dtCol);
                    dtCol = new DataColumn();
                    dtCol.ColumnName = "source";
                    valIMPFieldsAttr.Columns.Add(dtCol);
                    dtCol = new DataColumn();
                    dtCol.ColumnName = "target";
                    valIMPFieldsAttr.Columns.Add(dtCol);
                    dtCol = new DataColumn();
                    dtCol.ColumnName = "type";
                    valIMPFieldsAttr.Columns.Add(dtCol);
                    dtCol = new DataColumn();
                    dtCol.ColumnName = "parent";
                    valIMPFieldsAttr.Columns.Add(dtCol);

                    if (xEle.Element("attribute").HasElements)
                    {                    
                        foreach (XElement xEle2 in xEle.Element("attribute").Elements())
                        {
                            DataRow rowtbAtrr = valIMPFieldsAttr.NewRow();
                            rowtbAtrr["copy"] = xEle2.Element("copy").Value;
                            rowtbAtrr["name"] = xEle2.Element("name").Value;
                            rowtbAtrr["source"] = xEle2.Element("source").Value;
                            rowtbAtrr["target"] = xEle2.Element("target").Value;
                            rowtbAtrr["type"] = xEle2.Element("type").Value;
                            rowtbAtrr["parent"] = xEle.Element("name").Value;
                            valIMPFieldsAttr.Rows.Add(rowtbAtrr);
                            
                            DataRow rowData2 = this.mapFields.NewRow();
                            rowData2["copy"] = xEle2.Element("copy").Value;
                            rowData2["name"] = xEle2.Element("name").Value;
                            rowData2["source"] = xEle2.Element("source").Value;
                            rowData2["target"] = xEle2.Element("target").Value;
                            /* set parent do atributo */ 
                            rowData2["parent"] = xEle.Element("name").Value;
                            rowData2["type"] = xEle2.Element("type").Value;
                            rowData2["key"] = xEle.Element("key").Value;

                            this.mapFields.Rows.Add(rowData2);

                            try
                            {
                                    DataColumn col = new DataColumn();
                                    col.DataType = System.Type.GetType(xEle2.Element("type").Value);
                                    col.ColumnName = xEle2.Element("source").Value;
                                    this.dtSource.Columns.Add(col);
                            }
                            catch (Exception ex)
                            {
                                    Console.WriteLine("[Mapping Field XML] : Source Column : " + xEle.Element("source").Value + " Parent : " + XMLnodeParent + " " + ex);
                                    return false;
                            }

                        }
                        valIMPFieldsAttr.AcceptChanges();
                        valIMPFieldsAttr.Dispose();
                        rowData["attribute"] = valIMPFieldsAttr;
                }

                #endregion set colunas table child atritutos    

                    this.mapFields.Rows.Add(rowData);
                try
                {
                    DataColumn col = new DataColumn();
                    col.DataType = System.Type.GetType(xEle.Element("type").Value);
                    col.ColumnName = xEle.Element("source").Value;
                    this.dtSource.Columns.Add(col);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("[Mapping Field XML] : Source Column : " + xEle.Element("source").Value + " Parent : " + XMLnodeParent + " " + ex);
                    Utils.escreveLog("[Mapping Field XML] : Source Column : " + xEle.Element("source").Value + " Parent : " + XMLnodeParent + " " + ex, "ERRO");
                    return false;
                }              
            }

            this.mapFields.AcceptChanges();
            this.mapFields.Dispose();
            return true;
        }
            
        public void getTablesImport_v2(XElement xmlNodesMap, XElement xmlNodesData)
        {
            
            dynamic queryResult = from xNode in xmlNodesMap.Elements("table")
                                  where (string)xNode.Element("copy").Value == "1"
                                  select xNode;

            foreach (XElement xEle in queryResult)
            {
                string nodeRoot = xEle.Element("nodeRoot").Value;
                string nodeParent = xEle.Element("nodeParent").Value;
                string nodeChild = xEle.Element("nodeChild").Value;
                string DBtarget = xEle.Element("DBtarget").Value;
                string DBsourceTMP = xEle.Element("DBsourceTMP").Value;

                if (creatSourceMapFields(xmlNodesMap, nodeParent) == true)
                {                    
                    IEnumerable<dynamic> xmlNodeResult = getResultQueryXML(xmlNodesData, nodeRoot, nodeParent, nodeChild);

                    if (xmlNodeResult.Count() > 0)
                    {
                        this.CreatDataSource(DBtarget, DBsourceTMP, xmlNodeResult);
                        if (this.dtSource.Rows.Count > 0)
                            this.InsertSQLdata(DBtarget, DBsourceTMP);
                    }
                    else
                    {
                        //Console.WriteLine("[ERRO Source XML File] : target : " + DBtarget + " NodeParent " + nodeParent + " querylinq XMLSOURCE sem resultados");
                        //Utils.escreveLog("[ERRO Source XML File] : target : " + DBtarget + " NodeParent " + nodeParent,"ERRO");

                        /* NO IMOVEIS NAO EXISTE > MARCAR ACTIVO=0 */
                        if (DBtarget == "DB_IMOVEIS")                       
                            Utils.SQL_EXEC(BuildMergeSQL("TDB_IMOVEIS_TMP", "DB_IMOVEIS"), this.command, 0);

                        if (DBtarget == "EXPORTADORES_CLIENTES_IMOVEIS")
                            Utils.SQL_EXEC(BuildMergeSQL("T_EXPORTADORES_CLIENTES_IMOVEIS_TMP", "EXPORTADORES_CLIENTES_IMOVEIS"), this.command, 0);

                        continue;
                        //break;
                    }                       
                }
                else
                    break;               
            }
        }

        public dynamic getResultQueryXML(XElement xmlNodes, string nodeRoot, string nodeParent, string nodeChild)
        {
            dynamic queryResult = null;

            if (nodeParent == "agencias")
            {
                queryResult = from xNode in xmlNodes.Elements(nodeParent).Elements(nodeChild)
                              where (string)xNode.Element("id").Value == this.AgenciaId_Key
                              select xNode;
            }

            if (nodeParent == "imoveis")
            {

                queryResult = from xNode in xmlNodes.Elements("agencias").Elements("agencia").Elements("imoveis").Elements()
                              where (string)xNode.Parent.Parent.Element("id").Value == this.AgenciaId_Key
                              select xNode;
            }

            if (nodeParent == "exportadores")
            {
                queryResult = from xNode in xmlNodes.Elements("agencias").Elements("agencia").Elements("imoveis").Elements("imovel").Elements("exportadores").Elements()
                              where (string)xNode.Parent.Parent.Parent.Parent.Element("id").Value == this.AgenciaId_Key
                              select xNode;

            }

            if (nodeParent == "angariadores")
            {
                queryResult = from xNode in xmlNodes.Elements("agencias").Elements("agencia").Elements("imoveis").Elements("imovel").Elements("angariadores").Elements()
                              where (string)xNode.Parent.Parent.Parent.Parent.Element("id").Value == this.AgenciaId_Key
                              select xNode;

            }

            if (nodeParent == "proprietarios") // IMOVEIS_ENTIDADES
            {
                queryResult = from xNode in xmlNodes.Elements("agencias").Elements("agencia").Elements("imoveis").Elements("imovel").Elements("proprietarios").Elements()
                              where (string)xNode.Parent.Parent.Parent.Parent.Element("id").Value == this.AgenciaId_Key
                              select xNode;
            }

            if (nodeParent == "caracteristicas")
            {
                queryResult = from xNode in xmlNodes.Elements("agencias").Elements("agencia").Elements("imoveis").Elements("imovel").Elements("caracteristicas").Elements()
                              where (string)xNode.Parent.Parent.Parent.Parent.Element("id").Value == this.AgenciaId_Key
                              select xNode;
            }

            if (nodeParent == "divisoes")
            {
                queryResult = from xNode in xmlNodes.Elements("agencias").Elements("agencia").Elements("imoveis").Elements("imovel").Elements("divisoes").Elements()
                              where (string)xNode.Parent.Parent.Parent.Parent.Element("id").Value == this.AgenciaId_Key
                              select xNode;
            }

            if (nodeParent == "multimedia") 
            {
                queryResult = from xNode in xmlNodes.Elements("agencias").Elements("agencia").Elements("imoveis").Elements("imovel").Elements("multimedia").Elements()
                              where (string)xNode.Parent.Parent.Parent.Parent.Element("id").Value == this.AgenciaId_Key
                              select xNode;
            }

            if (nodeParent == "documentos") 
            {
                queryResult = from xNode in xmlNodes.Elements("agencias").Elements("agencia").Elements("imoveis").Elements("imovel").Elements("documentos").Elements()
                              where (string)xNode.Parent.Parent.Parent.Parent.Element("id").Value == this.AgenciaId_Key
                              select xNode;
            }

            if (nodeParent == "coordenadasExtra")
            {
                queryResult = from xNode in xmlNodes.Elements("agencias").Elements("agencia").Elements("imoveis").Elements("imovel").Elements("coordenadasExtra").Elements()
                              where (string)xNode.Parent.Parent.Parent.Parent.Element("id").Value == this.AgenciaId_Key
                              select xNode;
            }

            return queryResult;
        }

        public void CreatDataSource(string tb_DBSource, string tb_DBTarget_TMP, dynamic queryResultXML)
        {
            bool flagErro = false;

            // Criar DataTable Source com os dados do xml a importar
            foreach (XElement xEle in queryResultXML)
            {
                DataRow rowData = dtSource.NewRow();

                string RootKey = null;   /* agencia */
                string RootName = xEle.Parent.Name.ToString();
                string ParentName = xEle.Parent.Parent.Name.ToString();
                string ParentKey = null; /* imovel */

                if (ParentName == "empresa")                
                    ParentKey = xEle.Element("id").Value; // agenciaId = key node empresa
                                  
                if (ParentName == "agencia")               
                    ParentKey = xEle.Parent.Parent.Element("id").Value; // agenciaId = key 

                if (ParentName == "imovel")                
                    ParentKey = xEle.Parent.Parent.Element("id").Value; // imovelId = key
                   
                if (RootName == "imoveis")
                {
                    if (xEle.Elements("ref").Any() == false)
                        continue;
                }
                if (ParentName == "imovel" && RootName== "angariadores" || RootName == "proprietarios" || RootName == "exportadores" || RootName == "divisoes" || RootName == "caracteristicas" || RootName == "multimedia" || RootName == "documentos" || RootName == "coordenadasExtra")
                    RootKey = xEle.Parent.Parent.Parent.Parent.Element("id").Value; // agenciaId = key > angariadores

                // set colunas mapping              
                IEnumerable<DataRow> queryResultMap = from mapRow in mapFields.AsEnumerable()                                                      
                                                      where (string)mapRow["copy"] == "1" || (string)mapRow["key"] == "2"
                                                      select mapRow;

                foreach (DataRow rowMap in queryResultMap)
                {
                    string valuexEle = null;
                    string columnKey = rowMap.Field<string>("key");
                    string columnField = rowMap.Field<string>("name");
                    string columnSource = rowMap.Field<string>("source");
                    string columnTarget = rowMap.Field<string>("target");
                    DataTable columnAttribute = rowMap.Field<DataTable>("attribute");
                    string columnParent = rowMap.Field<string>("parent");
                    string columnType = rowMap.Field<string>("type");

                    /* id Cliente ximo redundante na table imoveis */
                    if (columnKey == "0" && columnTarget == "id_clienteXimo")
                    {
                        if (rowData.Table.Columns.Contains("id_clienteXimo"))
                        { 
                            rowData["id_clienteXimo"] = this.ClienteId_KeyXimo;
                            continue;
                        }
                    }

                    try
                    {
                        #region KEYS 
                        /* PARAMETROS KEYS FALTA UPDATE PARA RETIRAR ESTE BLOCO HARDCODE */

                        /* ID_CLIENTE - HARDCODE n está no xml incluir ou trocar pelo n de serie  */
                        if (columnKey == "2" && columnTarget == "id_cliente") /* id_cliente */ 
                        {
                            rowData[columnField] = this.ClienteId_Key;
                            continue;
                        }                        
                        /* ID_AGENCIA - HARDCODE */
                        if (columnKey == "2" && columnTarget == "id_agencia" && RootName== "imoveis")
                        {
                            rowData[columnField] = ParentKey;
                            continue;
                        }

                        if ((columnKey == "2" && columnTarget == "id_agencia")
                          && (RootName == "angariadores" || RootName == "proprietarios" || RootName == "exportadores" || RootName == "divisoes" || RootName == "caracteristicas" || RootName == "multimedia" || RootName == "documentos" || RootName == "coordenadasExtra"))
                        {
                            rowData[columnField] = RootKey;
                            continue;
                        }

                       /* ID_EXPORTADOR - HARDCODE */
                        if (columnKey == "2" && columnTarget == "id_exportador" && RootName == "exportadores")
                        {
                            if (xEle.HasAttributes)                           
                                rowData[columnField] = xEle.Attribute("id").Value;                           
                            continue;
                        }

                        /* ID_FOTO - HARDCODE */
                        if (columnKey == "2" && columnTarget == "id_fotoXimo" && RootName == "multimedia")
                        {
                            if(xEle.HasAttributes)
                               rowData[columnField] = xEle.Attribute("id").Value;
                            continue;
                        }
                        /* ID_DOC - HARDCODE */
                        if (columnKey == "2" && columnTarget == "id_docXimo" && RootName == "documentos")
                        {
                            if (xEle.HasAttributes)
                                rowData[columnField] = xEle.Attribute("id").Value;
                            continue;
                        }

                        /* ID_EXPORTADOR-COORDENADAS - HARDCODE */
                        if (columnKey == "2" && columnTarget == "id_exportador" && RootName == "coordenadasExtra")
                        {
                            if (xEle.Element("exportador").HasAttributes)
                                rowData[columnField] = xEle.Element("exportador").Attribute("id").Value;
                            continue;
                        }

                        /* ID_CARACT - HARDCODE */
                        if (columnKey == "2" && columnTarget == "IdCaract" && RootName == "caracteristicas")
                        {
                            if (xEle.Element("nome").HasAttributes)
                                rowData[columnField] = xEle.Element("nome").Attribute("id").Value;
                            continue;
                        }
                        /* ID_DIVISAO - HARDCODE */
                        if (columnKey == "2" && (columnTarget == "IdDivisao" || columnTarget == "IdImovelDivisao") && RootName == "divisoes")
                        {
                            if (xEle.Element("design").HasAttributes)
                            {
                                if(columnField=="id")
                                   rowData[columnField] = xEle.Element("design").Attribute("id").Value;

                                if (columnField == "key")                                   
                                    rowData[columnField] = xEle.Element("design").Attribute("key").Value;

                                continue;
                            }                         
                        }

                        /* ID_ANGARIADOR - HARDCODE */
                        if (columnKey == "2" && (columnTarget == "IdAngariador" || columnTarget == "IdImovelAngariador") && RootName == "angariadores")
                        {
                            if (xEle.HasAttributes)
                            {
                                if (columnField == "id")
                                    rowData[columnField] = xEle.Attribute("id").Value;

                                if (columnField == "key")
                                    rowData[columnField] = xEle.Attribute("key").Value;

                                continue;
                            }
                        }

                        #endregion KEYS 

                        /* key´s de de nodes parent´s imovel e agencia */
                        if (columnParent == ParentName)                  
                            rowData[columnField] = ParentKey;

                        else
                        {                                         
                            #region attributes nodes
                            if (columnParent != "0" && columnParent!= ParentName )
                            {
                                valuexEle = xEle.Element(columnParent).Attribute(columnField).Value;
                                columnField = columnSource;
                            }

                            #endregion attributes nodes
                            else                            
                                valuexEle = xEle.Element(columnField).Value;
                      
                            if (columnType == "System.Int32" && valuexEle != "")
                                rowData[columnField] = Convert.ToInt32(valuexEle);

                            if (columnType == "System.String" && valuexEle != "")
                                rowData[columnField] = Convert.ToString(valuexEle);

                            if (columnType == "System.Double" && valuexEle != "")
                                rowData[columnField] = Convert.ToDouble(valuexEle);

                            if (columnType == "System.DateTime" && valuexEle != "")
                                rowData[columnField] = Convert.ToDateTime(valuexEle);
                           
                        }                       
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(" [Cliente ExporDB] : " + this.ClienteId_Key + " [Cliente ImportDB] : "+ this.ClienteId_KeyXimo +" [Mapping Field XML] : " + valuexEle + " Node " + columnField + " [Source DATA] - Parent " + RootName + " IMOKEY " + ParentKey + " [ERRO] " + ex);
                        Utils.escreveLog(" [Cliente ExporDB] : " + this.ClienteId_Key + " [Cliente ImportDB] : " + this.ClienteId_KeyXimo +" - "+ valuexEle + " Node " + columnField + " [Source DATA] - Parent " + RootName + " IMOKEY " + ParentKey + " [ERRO] " + ex,"ERRO");
                        flagErro = true;
                        break;
                    }                  
                }

                if (flagErro==true)
                    break;

                dtSource.Rows.Add(rowData);
            }       
            dtSource.AcceptChanges();

        }

        public void InsertSQLdata(string tb_DBSource, string tb_DBTarget_TMP)
        {
         
            // Set up the bulk copy object.                
            SqlBulkCopy bulkCopy = new SqlBulkCopy(connection);    
            bulkCopy.DestinationTableName = tb_DBTarget_TMP;

            // Mapping columns dataTable XLMdata by name > sqlTable 
            string columnKey = null;
            string columnSource = null;
            string columnTarget = null;
            int i = 0;
            while (mapFields.Rows.Count > i)
            {
                columnKey = mapFields.Rows[i]["key"].ToString();
                columnSource = mapFields.Rows[i]["source"].ToString();
                columnTarget = mapFields.Rows[i]["target"].ToString();
             
                SqlBulkCopyColumnMapping mapCol = new SqlBulkCopyColumnMapping(columnSource, columnTarget);
                bulkCopy.ColumnMappings.Add(mapCol);              
                i++;
            }

            //connection.Open();
            Console.WriteLine(tb_DBSource);

            try
            {
                // 1 > INSERT TMP_TABLE Write unchanged rows from the source to the destination.
                bulkCopy.WriteToServer(dtSource, DataRowState.Unchanged);
                bulkCopy.Close();
                            
                // 2 > BUILD SQL MERGE > RUN AND CLEAN
                Utils.SQL_EXEC(BuildMergeSQL(tb_DBTarget_TMP, tb_DBSource),this.command,0);
             
               
            }catch (Exception ex) {
                
                string sqlcomand = BuildMergeSQL(tb_DBTarget_TMP, tb_DBSource);
                Console.WriteLine("[ERRO BULK SQLMERGE] : Cliente: " + this.ClienteId_Key + " - "+tb_DBSource + " "+ex.Message +" "+ sqlcomand);
                Utils.escreveLog("[ERRO BULK SQLMERGE] : Cliente: "+ this.ClienteId_Key +" - " +tb_DBSource + " "+ex.Message +" "+ sqlcomand, "ERRO");

                #region debug EX
                DataTable DBtable = Utils.SQL_DATA_TYPE(tb_DBTarget_TMP);

                foreach (DataRow row in this.mapFields.Rows)
                {
                    DataRow[] result = DBtable.Select(" COLUMN_NAME = '" + row["target"].ToString() + "'");

                    string source = result[0][0].ToString();
                    string target = row["target"].ToString();


                    if (source != target)
                    {
                        Console.WriteLine("[ERRO BULK SQLMERGE] target Map : " + target + " != DB table field :" + source + " table:" + tb_DBSource + " ", "");
                        Utils.escreveLog("[ERRO BULK SQLMERGE] target Map : " + target + " != DB table field :" + source + " table:" + tb_DBSource + " ", "");
                    }

                    if ((result[0][1].ToString() == "tinyint" || result[0][1].ToString() == "bigint" || result[0][1].ToString() == "int") && (row["type"].ToString() == "System.String" || row["type"].ToString() == "System.Double"))
                    {
                        Console.WriteLine("[ERRO BULK SQLMERGE] target Map :" + target + " source:" + result[0][1].ToString() + " table:" + tb_DBSource + " ", "");
                        Utils.escreveLog("[ERRO BULK SQLMERGE] target Map : " + target + " source:" + result[0][1].ToString() + " table:" + tb_DBSource + " ", "ERRO");
                    }

                    if (row["target"].ToString().Any(c => char.IsUpper(c)) != row["source"].ToString().Any(c => char.IsUpper(c)))
                    {
                        Console.WriteLine("[ERRO BULK SQLMERGE UPPERCASE] target Map :" + target + " source:" + result[0][1].ToString() + " table:" + tb_DBSource + " ", "");
                        Utils.escreveLog("[ERRO BULK SQLMERGE UPPERCASE] target Map : " + target + " source:" + result[0][1].ToString() + " table:" + tb_DBSource + " ", "ERRO");
                    }
                    else                       
                        Utils.escreveLog("[ERRO BULK SQLMERGE] target: " + row["target"].ToString() + " ", "ERRO");
                   
                }
               
                #endregion debug EX
            }
            mapFields.Dispose();
            dtSource.Dispose();
        }

        public string BuildMergeSQL(string tbSourceTMP, string tbTarget)
        {
            int i = 0, keys = 0; 
            int keysCount = mapFields.Select("Key = '1' OR Key = '2'").Length;
            string SqlKeys = "", SqlUpdate_Cols = "", SqlInsert_Cols = "", SqlInsert_Values = "";
                    
            IEnumerable<DataRow> queryMapFieldsSQL = from fieldMap in mapFields.AsEnumerable()
                                                     select fieldMap;

            foreach (DataRow rowMap in queryMapFieldsSQL)
            {
                #region UNIQUE - KEYS 
                // KEY = 1 >> ID UNIQUE KEY   
                if (rowMap.Field<string>("key") == "1" || rowMap.Field<string>("key") == "2") // ID unique key
                {
                    SqlKeys += "target." + rowMap.Field<string>("target") + " = source." + rowMap.Field<string>("target") + " ";

                    if (keys >= 0 && keys < keysCount - 1)
                        SqlKeys += " AND ";

                    keys++;
                }
                #endregion UNIQUE - KEYS

                #region UPDATE - FIELDS
                // KEY = 0 >> não é UNIQUE KEY da tabela >> nem FOREIGN KEY
                if (rowMap.Field<string>("key") == "0") // não é UNIQUE KEY da tabela >> nem FOREIGN KEY
                {
                    SqlUpdate_Cols += "target." + rowMap.Field<string>("target") + " = source." + rowMap.Field<string>("target") + " ";

                    if (i >= 0 && i < mapFields.Rows.Count - 1)
                        SqlUpdate_Cols += ",";
                }

                

                #endregion UPDATE - FIELDS

                #region INSERT - FIELDS
                // KEY != 1 (exclui UNIQUE KEYS) >> insert fields (0,2,3... FOREIGN KEY)  
                if (rowMap.Field<string>("key") != "1")
                {
                    
                    SqlInsert_Cols += rowMap.Field<string>("target") + " ";

                    if (i >= 0 && i < (mapFields.Rows.Count - 1))
                        SqlInsert_Cols += ",";

                    if (rowMap.Field<string>("key") == "0" || rowMap.Field<string>("key") == "2")
                    {                     
                        SqlInsert_Values += "source." + rowMap.Field<string>("target") + " ";                       
                    }

                    if (i >= 0 && i < ((mapFields.Rows.Count) - 1))
                        SqlInsert_Values += ",";
                }
                #endregion INSERT - FIELDS

                i++;
            }

            string SQLMerge = null;

            if( tbTarget == "DB_IMOVEIS")
            {               
                SQLMerge = " MERGE INTO " + tbTarget + "  WITH (HOLDLOCK) AS target" +
                           " USING " + tbSourceTMP + " AS source ON " + SqlKeys + " " +
                           " WHEN MATCHED THEN " +
                           " UPDATE SET " + SqlUpdate_Cols + "  ,activoExportacao=1,dataImport=GETDATE()" +
                           " WHEN NOT MATCHED BY TARGET THEN " +
                           " INSERT(" + SqlInsert_Cols + ",dataImport) VALUES (" + SqlInsert_Values + ",GETDATE())" +
                           " WHEN NOT MATCHED BY SOURCE AND id_cliente='" + ClienteId_Key + "' AND target.id_agencia='" + AgenciaId_Key + "' THEN UPDATE SET activoExportacao=0,inativoData=GETDATE();";                          
            }

            if (tbTarget == "CLIENTES_AGENCIAS" || tbTarget == "DB_IMOVEIS_ENTIDADES" )
            {
                SQLMerge = " MERGE INTO " + tbTarget + "  WITH (HOLDLOCK) AS target" +
                           " USING " + tbSourceTMP + " AS source ON " + SqlKeys + " " +
                           " WHEN MATCHED THEN " +
                           " UPDATE SET " + SqlUpdate_Cols + " " +
                           " WHEN NOT MATCHED BY TARGET THEN " +
                           " INSERT(" + SqlInsert_Cols + ") VALUES (" + SqlInsert_Values + ");";
            }

            /* DELETE ROW ELIMINADAS - FOTOS - DIVISOES - CARACTERISTICAS - DOCUMENTOS - COORDENADAS-PORTAIS */
            if ( tbTarget == "DB_IMOVEIS_ANGARIADORES" || tbTarget == "DB_IMOVEIS_FOTOS" || tbTarget == "DB_IMOVEIS_DIVISOES" || tbTarget == "DB_IMOVEIS_CARACTERISTICAS" || tbTarget == "DB_IMOVEIS_DOCUMENTOS" || tbTarget == "EXPORTADORES_CLIENTES_IMOVEIS_GEOREFS")
            {
                string update = "";
                if (SqlUpdate_Cols != "")
                    update = " WHEN MATCHED THEN UPDATE SET " + SqlUpdate_Cols + " " + " ";
               
                SQLMerge = " MERGE INTO " + tbTarget + "  WITH (HOLDLOCK) AS target" +
                           " USING " + tbSourceTMP + " AS source ON " + SqlKeys + " " +
                           " " + update + 
                           " WHEN NOT MATCHED BY TARGET THEN " +
                           " INSERT(" + SqlInsert_Cols + ") VALUES (" + SqlInsert_Values + ")" +                        
                           " WHEN NOT MATCHED BY SOURCE AND target.id_cliente='" + ClienteId_Key + "' AND target.id_agencia='" + AgenciaId_Key + "'" +
                           " THEN DELETE;";

            }

            /* update exportacao activa imovel > cliente > exportador  */
            if (tbTarget == "EXPORTADORES_CLIENTES_IMOVEIS")
            {
                SQLMerge = " MERGE INTO " + tbTarget + "  WITH (HOLDLOCK) AS target" +
                           " USING " + tbSourceTMP + " AS source ON " + SqlKeys + " " +
                           " WHEN NOT MATCHED BY TARGET THEN " +
                           " INSERT(" + SqlInsert_Cols + ",inseridoData) VALUES (" + SqlInsert_Values + ",GETDATE())" +                           
                           " WHEN NOT MATCHED BY SOURCE AND target.id_cliente='" + ClienteId_Key + "' AND target.id_agencia='" + AgenciaId_Key + "' AND activo=1" +
                           " THEN UPDATE SET activo=0,exportadoData='1900-01-01',removidoData=GETDATE(),removidoDataExportador='1900-01-01',validadoPortal=0,validadoDataPortal='1900-01-01' " + /* forçar data exportacao caso volte a ser selecionado para exportar */
                           " WHEN MATCHED THEN UPDATE SET activo = 1,editadoData = GETDATE();"; 
            }

            return SQLMerge;
        }
   
        public void getSqlTablesClean()
        {
            try
            {
                Utils.SQL_EXEC("TRUNCATE TABLE T_CLIENTES_AGENCIAS_TMP;" +
                               "TRUNCATE TABLE T_EXPORTADORES_CLIENTES_IMOVEIS_TMP;"+
                               "TRUNCATE TABLE TDB_IMOVEIS_TMP;"+
                               "TRUNCATE TABLE TDB_IMOVEIS_ANGARIADORES_TMP;"+
                               "TRUNCATE TABLE TDB_IMOVEIS_DIVISOES_TMP;"+
                               "TRUNCATE TABLE TDB_IMOVEIS_ENTIDADES_TMP;"+
                               "TRUNCATE TABLE TDB_IMOVEIS_CARACTERISTICAS_TMP;"+
                               "TRUNCATE TABLE TDB_IMOVEIS_FOTOS_TMP;"+
                               "TRUNCATE TABLE TDB_IMOVEIS_DOCUMENTOS_TMP;"+
                               "TRUNCATE TABLE T_EXPORTADORES_CLIENTES_IMOVEIS_GEOREFS_TMP;", this.command,0);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}






    



