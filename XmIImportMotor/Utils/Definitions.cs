﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Web;

namespace XmIImportMotor
{
    class Definitions
    {

        public string Filedir = "";
        string connection = "";
        public string errors = "";
        string log = "1";
        public string logsPath = "";
        string listview = "1";
        public string querys = "1";

        public string clienteId = "";
        public string numeroSerie = "";
        public string exportIdXimo = "";
        public string queryImport = "";
        public string queryExport = "";
        public string debugFlag = "";
        public string nextProcess_path_1 = ""; 


        public void setConnection()
        {

            Ini.IniFile file;
            if (File.Exists(Application.StartupPath + "/config.ini"))
            {
                file = new Ini.IniFile(Application.StartupPath + "/config.ini");
                connection = file.IniReadValue("GERAL", "CONNECTION");
                Filedir = file.IniReadValue("GERAL", "PATH");
                log = file.IniReadValue("GERAL", "LOG");
                logsPath = file.IniReadValue("GERAL", "LOGSPATH");
                listview = file.IniReadValue("GERAL", "LISTVIEW");
                querys = file.IniReadValue("GERAL", "QUERYS");
                clienteId = file.IniReadValue("GERAL", "CLIENTE");
                exportIdXimo = file.IniReadValue("GERAL", "EXPORTXIMO");
                queryImport = file.IniReadValue("QUERYS", "IMPORT");
                queryExport = file.IniReadValue("QUERYS", "EXPORT");
                debugFlag = file.IniReadValue("GERAL", "DEBUG");
                nextProcess_path_1 = file.IniReadValue("PROCESS", "NEXTPROCESSPATH");

                

                Utils.SetConnString(connection);               
            }
        }
    }        
}
