﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace XmIImportMotor
{
    public sealed class Utils
    {
       

        private Utils() { }

        public static void SetConnString(string con)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var connectionStringsSection = (ConnectionStringsSection)config.GetSection("connectionStrings");
            connectionStringsSection.ConnectionStrings["ConString"].ConnectionString = con;
            config.Save();
            ConfigurationManager.RefreshSection("connectionStrings");
        }
        public static string GetConnString() { return ConfigurationManager.ConnectionStrings["ConString"].ToString(); }   

        #region XML
        public static string GetNodeValue(string tagname, XmlDocument doc)
        {
            XmlNodeList node = doc.GetElementsByTagName(tagname);
            return (node.Count > 0) ? node[0].InnerText.Replace("'", "&#39;") : "";
        }
        public static int GetNodeExist(string tagname, XmlDocument doc)
        {
            XmlNodeList node = doc.GetElementsByTagName(tagname);
            return node.Count;
        }
        public static string GetNodeAttribute(string tagname, int atributo, XmlDocument doc)
        {
            XmlNodeList node = doc.GetElementsByTagName(tagname);
            return (node.Count > 0) ? (node[0].Attributes.Count > atributo) ? node[0].Attributes[atributo].InnerText : "" : "";
        }
        #endregion XML

        #region AUX
        public static List<string> List(params string[] args)
        {
            if (args.Length == 1)
            {
                if (Utils.validateType("int_list", args[0].ToString()))
                {
                    string[] str = args[0].Split(',');
                    string[] args2 = new string[str.Length];
                    for (int i = 0; i < str.Length; i++)
                    {
                        args2[i] = str[i].ToString();
                    }
                    return new List<string>(args2);
                }
            }
            return new List<string>(args);
        }
        public static string Corta_string(string txt, int tamanho, string terminacao)
        {
            if (txt.Length > tamanho)
            {
                txt = txt.Substring(0, tamanho);
                int index = txt.LastIndexOf(" ");
                if (index > 1)
                {
                    txt = txt.Substring(0, index);
                }
                txt = txt + terminacao;
            }
            return txt;
        }
        public static string REMOVE_ACCENTS(string texto)
        {
            texto = texto.ToLower().Replace("à", "a");
            texto = texto.ToLower().Replace("á", "a");
            texto = texto.ToLower().Replace("â", "a");
            texto = texto.ToLower().Replace("ã", "a");
            texto = texto.ToLower().Replace("ä", "a");
            texto = texto.ToLower().Replace("å", "a");
            texto = texto.ToLower().Replace("æ", "ae");
            texto = texto.ToLower().Replace("è", "e");
            texto = texto.ToLower().Replace("é", "e");
            texto = texto.ToLower().Replace("ê", "e");
            texto = texto.ToLower().Replace("ë", "e");
            texto = texto.ToLower().Replace("ì", "i");
            texto = texto.ToLower().Replace("í", "i");
            texto = texto.ToLower().Replace("î", "i");
            texto = texto.ToLower().Replace("ï", "i");
            texto = texto.ToLower().Replace("ò", "o");
            texto = texto.ToLower().Replace("ó", "o");
            texto = texto.ToLower().Replace("ô", "o");
            texto = texto.ToLower().Replace("õ", "o");
            texto = texto.ToLower().Replace("ö", "o");
            texto = texto.ToLower().Replace("ù", "u");
            texto = texto.ToLower().Replace("ú", "u");
            texto = texto.ToLower().Replace("û", "u");
            texto = texto.ToLower().Replace("ü", "u");
            texto = texto.ToLower().Replace("ç", "c");
            texto = texto.ToLower().Replace("ñ", "n");
            texto = texto.ToLower().Replace("ý", "y");
            texto = texto.ToLower().Replace("ÿ", "y");
            texto = texto.ToLower().Replace("ª", "");
            texto = texto.ToLower().Replace("º", "");
            texto = texto.ToLower().Replace(".", " ");
            texto = texto.ToLower().Replace(",", " ");
            texto = texto.ToLower().Replace("-", " ");
            texto = texto.ToLower().Replace("_", " ");
            texto = texto.ToLower().Replace("\"", "");
            texto = texto.ToLower().Replace("''", "");
            texto = texto.ToLower().Replace("´", "");
            texto = texto.ToLower().Replace("`", "");
            while (texto.IndexOf("  ") >= 0)
                texto = texto.ToLower().Replace("  ", " ");
            return texto;
        }
        public static string ValidateNum(string idcont)
        {
            string id = "0";
            if (!String.IsNullOrEmpty(idcont))
                id = idcont;
            return id;
        }
        public static bool validateType(string tipo, string valor)
        {

            if (String.IsNullOrEmpty(valor))
                return false;

            bool ok = false;
            switch (tipo)
            {
                case "int":
                    //Match m_int = Regex.Match(valor, @"([0-9]+)", RegexOptions.IgnoreCase);
                    //Match m_int = Regex.Match(valor, @"^-?[1-9]\d*$", RegexOptions.IgnoreCase);//negativos
                    Match m_int = Regex.Match(valor, @"^[0-9]\d*$", RegexOptions.IgnoreCase);
                    if (m_int.Success)
                        ok = true;
                    break;
                case "float":
                    Match m_float = Regex.Match(valor, @"^-?[0-9]+(\.[0-9]+)?$", RegexOptions.IgnoreCase);// ^-?(?:[0-9]+|[0-9]*\.[0-9]+)$ (aceita .3)
                    if (m_float.Success)
                        ok = true;
                    break;
                case "datetime":
                    DateTime testDate = DateTime.MinValue;
                    System.Data.SqlTypes.SqlDateTime sdt;
                    if (DateTime.TryParse(valor, out testDate))
                    {
                        try
                        {
                            sdt = new System.Data.SqlTypes.SqlDateTime(testDate);
                            ok = true;
                        }
                        catch (System.Data.SqlTypes.SqlTypeException)
                        {
                            ok = false;
                        }
                    }
                    break;
                case "int_list":
                    string[] temp = valor.Split(new char[] { ',' });
                    if (temp.Length > 0)
                        ok = true;
                    for (int i = 0; i < temp.Length; i++)
                    {
                        if (!validateType("int", temp[i]))
                            return false;
                    }
                    break;
                default:
                    break;
            }
            return ok;
        }
        //CUSTOM VALIDATORS
        /// <summary>
        /// Função para validar customValidators
        /// </summary>
        /// <param name="tipo">"string", "int", "email", "cod_postal"</param>
        /// <param name="input">Conteudo do input</param>
        /// <param name="valorInicial">Valor inicial do imput</param>
        /// <returns></returns>
        public static bool CustomValidator(string tipo, string input, string valorInicial)
        {
            if (input == valorInicial)
                return false;
            else if (input == "")
                return false;
            else if (tipo == "email")
            {
                if (isEmail(input) == false)
                    return false;
            }
            else if (tipo == "cod_postal")
            {
                if (isPostalCode(input) == false)
                    return false;
            }
            else if (tipo == "int")
            {
                try
                {
                    int x = Convert.ToInt32(input);
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else if (tipo == "telefone")
            {
                //validacao para numeros de telefone
                Regex numero = new Regex(@"^\d*\.?\d*$");
                return numero.IsMatch(input);
            }
            else if (tipo == "idade")
            {
                int x = 0;
                try
                {
                    x = Convert.ToInt32(input);
                }
                catch (Exception ex)
                {
                    return false;
                }
                if (x < 18 || x > 70)
                {
                    return false;
                }
            }
            return true;
        }
        public static bool isEmail(string inputEmail)
        {
            inputEmail = NulltoString(inputEmail);
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
            @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
            @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(inputEmail))
                return (true);
            else
                return (false);
        }
        public static bool isPostalCode(string inputPostalCode)
        {
            bool tipo1 = false;
            bool tipo2 = false;

            inputPostalCode = NulltoString(inputPostalCode);
            string strRegex = @"\d{4}-\d{3}?";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(inputPostalCode))
                tipo1 = true;
            else
                tipo1 = false;

            strRegex = @"^[0-9]{4}$";
            re = new Regex(strRegex);
            if (re.IsMatch(inputPostalCode))
                tipo2 = true;
            else
                tipo2 = false;

            if ((tipo1) || (tipo2))
                return true;
            else
                return false;
        }
        public static string NulltoString(string Str)
        {
            String ret;
            if (Str == null)
                ret = "";
            else
                ret = Str;
            return ret;
        }
        public static string replaceFloatParameter(string str)
        {
            if (String.IsNullOrEmpty(str))
                str = "0";
            str = str.Replace(".", ",");
            return str.Trim();
        }
        public static string replaceFloat(string str)
        {
            str = str.Replace(",", ".");
            return str.Trim();
        }
        #endregion AUX

        #region SQL
        public static SqlDataReader SQL_OPEN(string Query)
        {
            SqlConnection con = new SqlConnection(GetConnString());
            SqlCommand command = new SqlCommand();
            command.Connection = con;
            command.Connection.Open();
            command.CommandText = Query;
            return command.ExecuteReader(CommandBehavior.CloseConnection);
        }
        public static DataTable SQL_OPEN(string Query, SqlCommand command)
        {
            DataTable dt = new DataTable();
            if (command.Connection == null)
            {
                SqlConnection con = new SqlConnection(GetConnString());
                command.Connection = con;
                command.Connection.Open();
                command.CommandText = Query;
                dt.Load(command.ExecuteReader(CommandBehavior.CloseConnection));
                command.Parameters.Clear();

            }
            else
            {
                command.CommandText = Query;
                dt.Load(command.ExecuteReader());
                command.Parameters.Clear();
            }
            return dt;
        }
        public static void SQL_EXEC(string Query, SqlCommand command, int timeout)
        {
         
            if (command.Connection == null)
            {
                SqlConnection con = new SqlConnection(GetConnString());
                command.Connection = con;
                command.Connection.Open();
                command.CommandText = Query;              
                command.Parameters.Clear();

            }
            else
            {
                command.CommandText = Query;
                command.ExecuteNonQuery();
                command.CommandTimeout = timeout;
                command.Parameters.Clear();
            }                   
        }

       
        public static void SQL_EXEC(string Query, SqlCommand command, SqlTransaction transaction)
        {
            command.Transaction = transaction;
            command.CommandText = Query;
            command.ExecuteNonQuery();
            command.Parameters.Clear();
        }
        public static bool SQL_HASROWS(string Query)
        {
            bool rows = false;
            using (SqlConnection sourceConnection = new SqlConnection(GetConnString()))
            {
                sourceConnection.Open();
                SqlCommand Command = new SqlCommand(Query, sourceConnection);
                SqlDataReader reader = Command.ExecuteReader();
                rows = reader.HasRows;
                reader.Close();
            }
            return rows;
        }
        public static bool SQL_HASROWS(string Query, SqlCommand command)
        {
            bool rows = false;
            command.CommandText = Query;
            SqlDataReader reader = command.ExecuteReader();
            command.Parameters.Clear();
            rows = reader.HasRows;
            reader.Close();
            return rows;
        }
        public static int SQL_COUNT(string query)
        {
            int rows = 0;
            using (SqlConnection sourceConnection = new SqlConnection(GetConnString()))
            {
                sourceConnection.Open();
                SqlCommand Command = new SqlCommand(query, sourceConnection);
                rows = (int)Command.ExecuteScalar();
            }
            return rows;
        }
        public static int SQL_COUNT(string query, SqlCommand command)
        {
            int rows = 0;
            command.CommandText = query;
            rows = (int)command.ExecuteScalar();
            command.Parameters.Clear();
            return rows;
        }

        public static DataTable SQL_DATA_TYPE(string table)
        {
            DataTable dt = new DataTable();
            dt.Load(Utils.SQL_OPEN(" SELECT COLUMN_NAME, DATA_TYPE,CHARACTER_MAXIMUM_LENGTH,NUMERIC_PRECISION, DATETIME_PRECISION,IS_NULLABLE  FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + table + "' "));
            return dt;
        }
        #endregion SQL

        #region LOG
        public static void escreveLog(string text, string errors)
        {
            string dir = Application.StartupPath + "/" + "\\logs\\";
            string data_agora = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss:ffff");
            string data_file =  DateTime.Now.ToString("yyyyMMdd");
            Encoding enc = new UTF8Encoding(false);
            using (StreamWriter SW2 = new StreamWriter(dir + errors + data_file + ".log", true, enc))
            {
                SW2.WriteLine("[" + data_agora + "] " + text);
                SW2.Close();
            }
            //Application.DoEvents();
        }
        public static void escreveLog(string str, string query, SqlCommand command, Exception ex, string errors)
        {
            if (ex.Message.ToString().ToUpper().IndexOf("SELECT") == -1 && ex.Message.ToString().ToUpper().IndexOf("DELETE") == -1 && ex.Message.ToString().ToUpper().IndexOf("UPDATE") == -1)
                str = str.Replace("*query*", query);
            else
                str = str.Replace("*query*", ex.Message.ToString());
            str += "<br /><br /><br />" + ex.ToString();

            string str_param = command.CommandText;
            foreach (SqlParameter prm in command.Parameters)
            {
                str_param = str_param.Replace(" ", "§");
                str_param = str_param.Replace("§@" + prm.ParameterName.ToString() + "§", replaceFloat(prm.Value.ToString()));
                str_param = str_param.Replace("§§", "§").Replace("§", " ").Replace("  ", " ").Trim();
            }
            escreveLog(str + Environment.NewLine + "--------------DETALHES--------------" + Environment.NewLine + str_param + Environment.NewLine + "------------------------------------", errors);
        }

        public static void escreveLog(string text, string errors, string fileName)
        {
            string data_agora = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss:ffff");
            string data_file = DateTime.Now.ToString("yyyyMMdd");
            Encoding enc = new UTF8Encoding(false);
            using (StreamWriter SW2 = new StreamWriter(errors + fileName+"_"+data_file + ".log", true, enc))
            {
                SW2.WriteLine("[" + data_agora + "] " + text);
                SW2.Close();
            }
            //Application.DoEvents();
        }
        public static void escreveQueryLog(string query, SqlCommand command, string errors)
        {
            command.CommandText = query;
            string str_param = command.CommandText;
            str_param = str_param.Replace(" = @", "=@");
            str_param = str_param.Replace("= @", "=@");
            str_param = str_param.Replace(",", " ,");
            str_param = str_param.Replace("  ", " ");
            str_param = str_param.Replace(";", " ;");
            foreach (SqlParameter prm in command.Parameters)
            {
                str_param = str_param.Replace(" ", "§");
                string parametro = prm.ParameterName.ToString() + "§";
                string replace = replaceFloat(prm.Value.ToString());
                str_param = str_param.Replace(parametro, "'" + replace + "'§");
                str_param = str_param.Replace("§§", "§").Replace("§", " ").Replace("  ", " ").Trim();
            }
            str_param = str_param.Replace(" ,", ",");
            str_param = str_param.Replace(" ;", ";");
            if (str_param.IndexOf("@") != -1)
            {
                foreach (SqlParameter prm in command.Parameters)
                {
                    string parametro = prm.ParameterName.ToString();
                    string replace = replaceFloat(prm.Value.ToString());
                    str_param = str_param.Replace(parametro, "'" + replace + "'");
                }
            }
            escreveLog("Query: " + str_param, errors);
        }

        public static void EscreveXML(string text, string errors)
        {
            string data_agora = DateTime.Now.ToString("yyyyMMddHHmmss");
            string data_file = DateTime.Now.ToString("yyyyMMdd");
            Encoding enc = new UTF8Encoding(false);
            using (StreamWriter SW2 = new StreamWriter(errors + "log" + data_file + ".xml", true, enc))
            {
                SW2.WriteLine(text);
                SW2.Close();
            }
            //Application.DoEvents();
        }
        #endregion LOG
    }
}




