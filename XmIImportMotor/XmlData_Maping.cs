﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace XmIImportMotor
{
    class XmlData_Maping
    {

        public DataTable dtSource;
        public DataTable mapFields;
        XElement xmlNodesMap = null;

        /* SET TABLES COLUMNS TYPES */
        public void creatSourceTbMap(string XMLnodeParent)
        {

            dtSource = new DataTable();
            mapFields = new DataTable();

            #region set colunas DataSource
            DataColumn dtCol = new DataColumn();
            dtCol.ColumnName = "copy";
            this.mapFields.Columns.Add(dtCol);
            dtCol = new DataColumn();
            dtCol.ColumnName = "name";
            this.mapFields.Columns.Add(dtCol);
            dtCol = new DataColumn();
            dtCol.ColumnName = "source";
            this.mapFields.Columns.Add(dtCol);
            dtCol = new DataColumn();
            dtCol.ColumnName = "target";
            this.mapFields.Columns.Add(dtCol);
            dtCol = new DataColumn();
            dtCol.ColumnName = "attribute";
            dtCol.DataType = typeof(DataTable);
            this.mapFields.Columns.Add(dtCol);
            dtCol = new DataColumn();
            dtCol.ColumnName = "parent";
            this.mapFields.Columns.Add(dtCol);
            dtCol = new DataColumn();
            dtCol.ColumnName = "type";
            this.mapFields.Columns.Add(dtCol);
            dtCol = new DataColumn();
            dtCol.ColumnName = "key";
            this.mapFields.Columns.Add(dtCol);
            dtCol = new DataColumn();
            dtCol.ColumnName = "default";
            this.mapFields.Columns.Add(dtCol);
            dtCol = new DataColumn();
            dtCol.ColumnName = "values";
            this.mapFields.Columns.Add(dtCol);
            dtCol = new DataColumn();
            dtCol.ColumnName = "child";
            dtCol.DataType = typeof(DataTable);
            this.mapFields.Columns.Add(dtCol);
            dtCol = new DataColumn();
            dtCol.ColumnName = "tablelink";
            dtCol.DataType = typeof(DataTable);
            this.mapFields.Columns.Add(dtCol);

            #endregion set colunas DataSource

            dynamic queryResult = from xNode in xmlNodesMap.Elements(XMLnodeParent).Elements("column")
                                  where (string)xNode.Element("copy") == "1" || (string)xNode.Element("key") == "1"
                                  select xNode;

            foreach (XElement xEle in queryResult)
            {
                DataRow rowData = this.mapFields.NewRow();
                rowData["copy"] = xEle.Element("copy").Value;
                rowData["name"] = xEle.Element("name").Value;
                rowData["source"] = xEle.Element("source").Value;
                rowData["target"] = xEle.Element("target").Value;
                rowData["parent"] = xEle.Element("parent").Value;
                rowData["type"] = xEle.Element("type").Value;
                rowData["key"] = xEle.Element("key").Value;
                rowData["default"] = xEle.Element("default").Value;
                rowData["values"] = xEle.Element("values").Value;

                #region set colunas node <attribute>    
                DataTable valFieldsAttr = new DataTable();

                dtCol = new DataColumn();
                dtCol.ColumnName = "copy";
                valFieldsAttr.Columns.Add(dtCol);
                dtCol = new DataColumn();
                dtCol.ColumnName = "name";
                valFieldsAttr.Columns.Add(dtCol);
                dtCol = new DataColumn();
                dtCol.ColumnName = "source";
                valFieldsAttr.Columns.Add(dtCol);
                dtCol = new DataColumn();
                dtCol.ColumnName = "target";
                valFieldsAttr.Columns.Add(dtCol);
                dtCol = new DataColumn();
                dtCol.ColumnName = "type";
                valFieldsAttr.Columns.Add(dtCol);
                dtCol = new DataColumn();
                dtCol.ColumnName = "parent";
                valFieldsAttr.Columns.Add(dtCol);
                dtCol = new DataColumn();
                dtCol.ColumnName = "increment";
                valFieldsAttr.Columns.Add(dtCol);
                dtCol = new DataColumn();
                dtCol.ColumnName = "default";
                valFieldsAttr.Columns.Add(dtCol);

                if (xEle.Element("attribute").HasElements)
                {
                    foreach (XElement xEle2 in xEle.Element("attribute").Elements())
                    {
                        if (xEle2.Element("copy").Value == "1")
                        {
                            DataRow rowtbAtrr = valFieldsAttr.NewRow();
                            rowtbAtrr["copy"] = xEle2.Element("copy").Value;
                            rowtbAtrr["name"] = xEle2.Element("name").Value;
                            rowtbAtrr["source"] = xEle2.Element("source").Value;
                            rowtbAtrr["target"] = xEle2.Element("target").Value;
                            rowtbAtrr["type"] = xEle2.Element("type").Value;
                            rowtbAtrr["parent"] = xEle.Element("name").Value;
                            rowtbAtrr["default"] = xEle2.Element("default").Value;
                            valFieldsAttr.Rows.Add(rowtbAtrr);
                        }

                        try
                        {
                            DataColumn col = new DataColumn();
                            col.DataType = System.Type.GetType(xEle2.Element("type").Value);
                            col.ColumnName = xEle2.Element("source").Value;
                            this.dtSource.Columns.Add(col);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("[Mapping Field XML] : Source Column : " + xEle.Element("source").Value + " Parent : " + XMLnodeParent + " " + ex);                            
                        }

                    }
                    valFieldsAttr.AcceptChanges();
                    valFieldsAttr.Dispose();
                    rowData["attribute"] = valFieldsAttr;
                }

                #endregion set colunas node <attritute>    

                #region set colunas node <child>    
                DataTable mapFieldsChild = new DataTable();

                dtCol = new DataColumn();
                dtCol.ColumnName = "copy";
                mapFieldsChild.Columns.Add(dtCol);
                dtCol = new DataColumn();
                dtCol.ColumnName = "name";
                mapFieldsChild.Columns.Add(dtCol);
                dtCol = new DataColumn();
                dtCol.ColumnName = "source";
                mapFieldsChild.Columns.Add(dtCol);
                dtCol = new DataColumn();
                dtCol.ColumnName = "target";
                mapFieldsChild.Columns.Add(dtCol);
                dtCol = new DataColumn();
                dtCol.ColumnName = "attribute";
                dtCol.DataType = typeof(DataTable);
                mapFieldsChild.Columns.Add(dtCol);
                //dtCol = new DataColumn();
                //dtCol.ColumnName = "attribute";
                //mapFieldsChild.Columns.Add(dtCol);
                dtCol = new DataColumn();
                dtCol.ColumnName = "parent";
                mapFieldsChild.Columns.Add(dtCol);
                dtCol = new DataColumn();
                dtCol.ColumnName = "type";
                mapFieldsChild.Columns.Add(dtCol);
                dtCol = new DataColumn();
                dtCol.ColumnName = "default";
                mapFieldsChild.Columns.Add(dtCol);

                if (xEle.Element("child").HasElements)
                {
                    foreach (XElement xEle3 in xEle.Element("child").Elements())
                    {
                        if (xEle3.Element("copy").Value == "1")
                        {
                            DataRow rowtbChild = mapFieldsChild.NewRow();
                            rowtbChild["copy"] = xEle3.Element("copy").Value;
                            rowtbChild["name"] = xEle3.Element("name").Value;
                            rowtbChild["source"] = xEle3.Element("source").Value;
                            rowtbChild["target"] = xEle3.Element("target").Value;
                            rowtbChild["parent"] = xEle3.Element("parent").Value;
                            rowtbChild["type"] = xEle3.Element("type").Value;
                            rowtbChild["default"] = xEle3.Element("default").Value;
                            mapFieldsChild.Rows.Add(rowtbChild);

                            #region set atributos node child    
                            DataTable valFields_ChildAttr = new DataTable();

                            dtCol = new DataColumn();
                            dtCol.ColumnName = "copy";
                            valFields_ChildAttr.Columns.Add(dtCol);
                            dtCol = new DataColumn();
                            dtCol.ColumnName = "name";
                            valFields_ChildAttr.Columns.Add(dtCol);
                            dtCol = new DataColumn();
                            dtCol.ColumnName = "source";
                            valFields_ChildAttr.Columns.Add(dtCol);
                            dtCol = new DataColumn();
                            dtCol.ColumnName = "target";
                            valFields_ChildAttr.Columns.Add(dtCol);
                            dtCol = new DataColumn();
                            dtCol.ColumnName = "type";
                            valFields_ChildAttr.Columns.Add(dtCol);
                            dtCol = new DataColumn();
                            dtCol.ColumnName = "default";
                            valFields_ChildAttr.Columns.Add(dtCol);

                            if (xEle3.Element("attribute").HasElements)
                            {
                                foreach (XElement xEle3_1 in xEle3.Element("attribute").Elements())
                                {
                                    if (xEle3.Element("copy").Value == "1")
                                    {
                                        DataRow rowtbAtrr = valFields_ChildAttr.NewRow();
                                        rowtbAtrr["copy"] = xEle3_1.Element("copy").Value;
                                        rowtbAtrr["name"] = xEle3_1.Element("name").Value;
                                        rowtbAtrr["source"] = xEle3_1.Element("source").Value;
                                        rowtbAtrr["target"] = xEle3_1.Element("target").Value;
                                        rowtbAtrr["type"] = xEle3_1.Element("type").Value;
                                        rowtbAtrr["default"] = xEle3_1.Element("default").Value;
                                        valFields_ChildAttr.Rows.Add(rowtbAtrr);
                                    }

                                    try
                                    {
                                        DataColumn col = new DataColumn();
                                        col.DataType = System.Type.GetType(xEle3_1.Element("type").Value);
                                        col.ColumnName = xEle3_1.Element("source").Value;
                                        this.dtSource.Columns.Add(col);
                                    }
                                    catch (Exception ex)
                                    {
                                        Console.WriteLine("[Mapping Field XML] : Source Column : " + xEle.Element("source").Value + " Parent : " + XMLnodeParent + " " + ex);
                                        //return false;
                                    }

                                }
                                valFields_ChildAttr.AcceptChanges();
                                valFields_ChildAttr.Dispose();
                                rowtbChild["attribute"] = valFields_ChildAttr;
                            }
                            #endregion set colunas table child atritutos node do tablelink       
                        }

                        try
                        {
                            DataColumn col = new DataColumn();
                            col.DataType = System.Type.GetType(xEle3.Element("type").Value);
                            col.ColumnName = xEle3.Element("source").Value;
                            this.dtSource.Columns.Add(col);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("[Mapping Field XML] : Source Column : " + xEle.Element("source").Value + " Parent : " + XMLnodeParent + " " + ex);
                            //return false;
                        }
                    }
                    mapFieldsChild.AcceptChanges();
                    mapFieldsChild.Dispose();
                    rowData["child"] = mapFieldsChild;
                }

                #endregion set colunas node <child>   

                #region set colunas join table <tablelink>  
                DataTable mapFieldsTLink = new DataTable();

                dtCol = new DataColumn();
                dtCol.ColumnName = "copy";
                mapFieldsTLink.Columns.Add(dtCol);
                dtCol = new DataColumn();
                dtCol.ColumnName = "name";
                mapFieldsTLink.Columns.Add(dtCol);
                dtCol = new DataColumn();
                dtCol.ColumnName = "key";
                mapFieldsTLink.Columns.Add(dtCol);
                dtCol = new DataColumn();
                dtCol.ColumnName = "source";
                mapFieldsTLink.Columns.Add(dtCol);
                dtCol = new DataColumn();
                dtCol.ColumnName = "target";
                mapFieldsTLink.Columns.Add(dtCol);
                dtCol = new DataColumn();
                dtCol.ColumnName = "parent";
                mapFieldsTLink.Columns.Add(dtCol);

                dtCol = new DataColumn();
                dtCol.ColumnName = "attribute";
                dtCol.DataType = typeof(DataTable);
                mapFieldsTLink.Columns.Add(dtCol);

                dtCol = new DataColumn();
                dtCol.ColumnName = "table";
                mapFieldsTLink.Columns.Add(dtCol);
                dtCol = new DataColumn();
                dtCol.ColumnName = "type";
                mapFieldsTLink.Columns.Add(dtCol);
                dtCol = new DataColumn();
                dtCol.ColumnName = "default";
                mapFieldsTLink.Columns.Add(dtCol);
                dtCol = new DataColumn();
                dtCol.ColumnName = "values";
                mapFieldsTLink.Columns.Add(dtCol);


                if (xEle.Element("tablelink").HasElements)
                {
                    dynamic queryTlink = from xNodeChild in xEle.Element("tablelink").Elements()
                                         where (string)xNodeChild.Element("copy") == "1"
                                         select xNodeChild;

                    foreach (XElement xEle3 in queryTlink)
                    {
                        DataRow rowtbLink = mapFieldsTLink.NewRow();
                        rowtbLink["copy"] = xEle3.Element("copy").Value;
                        rowtbLink["key"] = xEle3.Element("key").Value;
                        rowtbLink["source"] = xEle3.Element("source").Value;
                        rowtbLink["target"] = xEle3.Element("target").Value;
                        rowtbLink["parent"] = xEle3.Element("parent").Value;
                        rowtbLink["table"] = xEle3.Parent.Attribute("table").Value;
                        rowtbLink["type"] = xEle3.Element("type").Value;
                        rowtbLink["default"] = xEle3.Element("default").Value;
                        rowtbLink["values"] = xEle3.Element("values").Value;

                        #region set colunas table child atributos node do tablelink    
                        DataTable valFields_TlinkAttr = new DataTable();

                        dtCol = new DataColumn();
                        dtCol.ColumnName = "copy";
                        valFields_TlinkAttr.Columns.Add(dtCol);
                        dtCol = new DataColumn();
                        dtCol.ColumnName = "name";
                        valFields_TlinkAttr.Columns.Add(dtCol);
                        dtCol = new DataColumn();
                        dtCol.ColumnName = "source";
                        valFields_TlinkAttr.Columns.Add(dtCol);
                        dtCol = new DataColumn();
                        dtCol.ColumnName = "target";
                        valFields_TlinkAttr.Columns.Add(dtCol);
                        dtCol = new DataColumn();
                        dtCol.ColumnName = "type";
                        valFields_TlinkAttr.Columns.Add(dtCol);
                        dtCol = new DataColumn();
                        dtCol.ColumnName = "parent";
                        valFields_TlinkAttr.Columns.Add(dtCol);
                        dtCol = new DataColumn();
                        dtCol.ColumnName = "default";
                        valFields_TlinkAttr.Columns.Add(dtCol);

                        if (xEle3.Element("attribute").HasElements)
                        {
                            foreach (XElement xEle4 in xEle3.Element("attribute").Elements())
                            {
                                if (xEle3.Element("copy").Value == "1")
                                {
                                    DataRow rowtbAtrr = valFields_TlinkAttr.NewRow();
                                    rowtbAtrr["copy"] = xEle4.Element("copy").Value;
                                    rowtbAtrr["name"] = xEle4.Element("name").Value;
                                    rowtbAtrr["source"] = xEle4.Element("source").Value;
                                    rowtbAtrr["target"] = xEle4.Element("target").Value;
                                    rowtbAtrr["type"] = xEle4.Element("type").Value;
                                    //rowtbAtrr["parent"] = xEle3.Element("name").Value;
                                    rowtbAtrr["default"] = xEle4.Element("default").Value;
                                    valFields_TlinkAttr.Rows.Add(rowtbAtrr);
                                }

                                try
                                {
                                    DataColumn col = new DataColumn();
                                    col.DataType = System.Type.GetType(xEle4.Element("type").Value);
                                    col.ColumnName = xEle4.Element("source").Value;
                                    this.dtSource.Columns.Add(col);
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine("[Mapping Field XML] : Source Column : " + xEle.Element("source").Value + " Parent : " + XMLnodeParent + " " + ex);
                                    //return false;
                                }

                            }
                            valFields_TlinkAttr.AcceptChanges();
                            valFields_TlinkAttr.Dispose();
                            rowtbLink["attribute"] = valFields_TlinkAttr;
                        }
                        #endregion set colunas table child atritutos node do tablelink       

                        mapFieldsTLink.Rows.Add(rowtbLink);
                    }
                    mapFieldsTLink.AcceptChanges();
                    mapFieldsTLink.Dispose();
                    rowData["tablelink"] = mapFieldsTLink;
                }
                #endregion set colunas join table <tablelink>

                this.mapFields.Rows.Add(rowData);
            }

            this.mapFields.AcceptChanges();
            this.mapFields.Dispose();
        }

    }
}
